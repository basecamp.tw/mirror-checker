#!/usr/bin/env python3

import sys
import requests
import hashlib

user_agent = {'User-agent': 'basecamp.tw mirror-checker'}

mirrors = [
    # "http://rudy.basecamp.tw",
    "http://rudy-direct.basecamp.tw",
    "http://moi.kcwu.csie.org",
    "http://map.happyman.idv.tw/rudy"
]

files = [
	"beta.html",
	"drops/beta.html",
	"drops/files.html",
	"drops/gts/index.html",
	"drops/local.html",
	"drops/taiwan_topo.html",
	"files.html",
	"gts/index.html",
	"local.html",
	"taiwan_topo.html",

    "legend_V1R1.pdf",

	"drops/md5sum.txt",
	"md5sum.txt",

	"drops/locus_all-happyman.xml",
	"drops/locus_all-kcwu.xml",
	"drops/locus_all-local.xml",
	"drops/locus_all-rex.xml",
	"drops/locus_all.xml",
	"drops/locus_dem-happyman.xml",
	"drops/locus_dem-kcwu.xml",
	"drops/locus_dem-local.xml",
	"drops/locus_dem-rex.xml",
	"drops/locus_dem.xml",
	"drops/locus_map-happyman.xml",
	"drops/locus_map-kcwu.xml",
	"drops/locus_map-local.xml",
	"drops/locus_map-rex.xml",
	"drops/locus_map.xml",
	"drops/locus_style-happyman.xml",
	"drops/locus_style-kcwu.xml",
	"drops/locus_style-local.xml",
	"drops/locus_style-rex.xml",
	"drops/locus_style.xml",
	"drops/locus_upgrade-happyman.xml",
	"drops/locus_upgrade-kcwu.xml",
	"drops/locus_upgrade-local.xml",
	"drops/locus_upgrade-rex.xml",
	"drops/locus_upgrade.xml",
	"locus_all-happyman.xml",
	"locus_all-kcwu.xml",
	"locus_all-local.xml",
	"locus_all-rex.xml",
	"locus_dem-happyman.xml",
	"locus_dem-kcwu.xml",
	"locus_dem-local.xml",
	"locus_dem-rex.xml",
	"locus_map-happyman.xml",
	"locus_map-kcwu.xml",
	"locus_map-local.xml",
	"locus_map-rex.xml",
	"locus_style-happyman.xml",
	"locus_style-kcwu.xml",
	"locus_style-local.xml",
	"locus_style-rex.xml",
	"locus_upgrade-happyman.xml",
	"locus_upgrade-kcwu.xml",
	"locus_upgrade-local.xml",
	"locus_upgrade-rex.xml",

	"Compartment.map.zip",
	"drops/hgt90-2019.2.zip",
	"drops/hgt90.zip",
	"drops/hgtmix-2019.2.zip",
	"drops/hgtmix.zip",
	"drops/moi-hgt-v2018.zip",
	"drops/MOI_OSM_bn_style.zip",
	"drops/MOI_OSM_dn_style.zip",
	"drops/MOI_OSM_extra_style.zip",
	"drops/MOI_OSM_Taiwan_TOPO_Lite_style.zip",
	"drops/MOI_OSM_Taiwan_TOPO_Rudy_hs_style.zip",
	"drops/MOI_OSM_Taiwan_TOPO_Rudy_locus_style.zip",
	"drops/MOI_OSM_Taiwan_TOPO_Rudy.map.zip",
	"drops/MOI_OSM_Taiwan_TOPO_Rudy_style.zip",
	"drops/MOI_OSM_tn_style.zip",
	"drops/MOI_OSM_twmap_style.zip",
	"drops/Taiwan_moi_zh_camp3D.gmap.zip",
	"drops/Taiwan_moi_zh_exp2.gmap.zip",
	"drops/Taiwan_moi_zh_exp.gmap.zip",
	"extra/Compartment.map.zip",
	"extra/Markchoo.map.zip",
	"gmapdem.img.zip",
	"gmapsupp_Taiwan_moi_zh_bw3D.img.zip",
	"gmapsupp_Taiwan_moi_zh_bw.img.zip",
	"gmapsupp_Taiwan_moi_zh_camp3D.img.zip",
	"gmapsupp_Taiwan_moi_zh_camp.img.zip",
	"gmapsupp_Taiwan_moi_zh_odc3D.img.zip",
	"gmapsupp_Taiwan_moi_zh_odc.img.zip",
	"hgt90-2019.1.zip",
	"hgt90.zip",
	"hgtmix-2019.1.zip",
	"hgtmix.zip",
	"moi3-srtm3-hgt.zip",
	"MOI_OSM_bn_style.zip",
	"MOI_OSM_dn_style.zip",
	"MOI_OSM_extra_style.zip",
	"MOI_OSM_Taiwan_TOPO_Lite.map.zip",
	"MOI_OSM_Taiwan_TOPO_Lite_style.zip",
	"MOI_OSM_Taiwan_TOPO_Lite.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy_hs_style.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy_locus_style.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy.map.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy.poi.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy_style.zip",
	"MOI_OSM_Taiwan_TOPO_Rudy.zip",
	"MOI_OSM_tn_style.zip",
	"MOI_OSM_twmap_style.zip",
	"Taiwan_moi_zh_bw3D.gmap.zip",
	"Taiwan_moi_zh_bw.gmap.zip",
	"Taiwan_moi_zh_camp3D.gmap.zip",
	"Taiwan_moi_zh_camp.gmap.zip",
	"Taiwan_moi_zh_exp2.gmap.zip",
	"Taiwan_moi_zh_exp.gmap.zip",
	"Taiwan_moi_zh_odc3D.gmap.zip",
	"Taiwan_moi_zh_odc.gmap.zip"

]

def retrieve_uri_headers(uri):
    try:
        response = requests.head(uri, headers=user_agent, timeout=(3.0, 300.0))
        response.raise_for_status()
        return response.headers
    except IOError as err:
        raise (err)

def retrieve_uri_checksum(uri):
    try:
        response = requests.get(uri, headers=user_agent, timeout=(3.0, 5.0))
        response.raise_for_status()
        response.headers['SHA256'] = hashlib.sha256(response.content).hexdigest()
        return response.headers
    except IOError as err:
        raise (err)

def checkheader(header1, header2, tag):
    if(tag not in header1 or tag not in header2):
        return False
    return header1[tag] == header2[tag]

def compare_headers(headers, tag):
    aligned = True
    previous_value = ''
    uris = list(headers)
    for i in range(len(uris)):
        try:
            if i > 0:
                aligned &= (previous_value == headers[uris[i]][tag])
            
            previous_value = headers[uris[i]][tag]
        except KeyError:
            previous_value = ''
    return aligned

def print_headers(headers):
    # Only display delta
    for key in ("SHA256", "Content-Type", "Date", "Content-Length", "Last-Modified", "Age"):
        if compare_headers(headers, key) is not True:
            for uri in sorted(list(headers)):
                if key in headers[uri]:
                    print("%-80s\t%-10s\t%-64s" % (uri, key, headers[uri][key]))
                else:
                    print("%-80s\t%-10s\t%-64s" % (uri, key, 'Not Set'))

if __name__ == "__main__":
    insync = True
    for filename in files:
        headers = {}
        aligned = True
        for host in mirrors:
            uri = "%s/%s" % (host, filename)

            if(filename.endswith(".html") or filename.endswith(".xml") or filename.endswith(".txt")):
                headers[uri] = retrieve_uri_checksum(uri)
                aligned &= compare_headers(headers, 'SHA256')
            else:
                headers[uri] = retrieve_uri_headers(uri)
                aligned &= compare_headers(headers, 'Last-Modified')
                aligned &= compare_headers(headers, 'Content-Length')

        if not aligned:
            print_headers(headers)
        insync &= aligned

    if insync != True:
        raise Exception ("mirrors are not in sync.")
